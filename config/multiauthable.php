<?php

return [
    'identity'      => [
        'default'   => 'username',
        'adapters'  => [
            'username'      => Nurmanhabib\MultiAuthable\Identities\Username::class,
            'email'         => Nurmanhabib\MultiAuthable\Identities\Email::class,
            'phone_number'  => Nurmanhabib\MultiAuthable\Identities\PhoneNumber::class,
        ]
    ],

    'checkpoint'    => [
        'enable'        => false,
        'checkpoints'   => [
            // App\Modules\Auth\Checkpoints\ActivationCheckpoint::class,
            // App\Modules\Auth\Checkpoints\BannedCheckpoint::class,
        ]
    ],

    'models'    => [
        'user'          => App\User::class,
        'user_identity' => Nurmanhabib\MultiAuthable\Models\UserIdentity::class,
    ],
];