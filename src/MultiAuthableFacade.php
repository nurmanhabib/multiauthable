<?php

namespace Nurmanhabib\MultiAuthable;

use Illuminate\Support\Facades\Facade;

class MultiAuthableFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'multiauthable';
    }
}
