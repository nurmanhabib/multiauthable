<?php

namespace Nurmanhabib\MultiAuthable;

use Nurmanhabib\MultiAuthable\Contracts\CheckpointContract;
use Nurmanhabib\MultiAuthable\Contracts\UserableContract;
use Nurmanhabib\MultiAuthable\Contracts\ProfileableContract;

class MultiAuthable
{
    protected $config;
    protected $auth;
    protected $dispatcher;
    protected $hasher;
    protected $models;

    protected $checkpoint_status = true;
    protected $checkpoints = [];

    public function __construct($app)
    {
        $this->config = $app['config']->get('multiauthable');
        $this->auth = $app['auth'];
        $this->dispatcher = $app['events'];
        $this->hasher = $app['hash'];

        $this->models = array_get($this->config, 'models', []);
    }

    public function authenticate(array $credentials, $type = null, $remember = false)
    {
        $response = $this->fireEvent('authenticating', compact('credentials'), true);

        if ($response === false) {
            return false;
        }

        $credentials    = new Credentials($credentials, $type);
        $identity       = $credentials->getIdentity();
        $password       = $credentials->getPassword();
        $userIdentity   = $this->makeModel('user_identity')->where(compact('identity'))->first();

        if ($userIdentity) {
            $user = $userIdentity->user;

            if ($this->checkPassword($user, $password)) {
                $response = $this->cycleCheckpoint($user);

                if ($response) {
                    $this->login($user, $remember);

                    $this->fireEvent('authenticated', compact('user'));

                    return $user;
                } else {
                    return $this->failedResponse('auth.failed');
                }
            } else {
                return $this->failedResponse('auth.failed');
            }
        } else {
            return $this->failedResponse('auth.failed');
        }
    }

    public function login($user, $remember = false)
    {
        $user_class_name = $this->models['user'];

        if (!($user instanceof $user_class_name)) {
            $user = $this->makeModel('user')->find($user);
        }
        
        $this->auth->login($user, $remember);
        
        return $user;
    }

    public function user()
    {
        return $this->auth->user();
    }

    public function checkPassword(UserableContract $user, $password)
    {
        return $this->hasher->check($password, $user->password);
    }

    public function addCheckpoint(CheckpointContract $checkpoint)
    {
        $this->checkpoints[] = $checkpoint;

        return $this;
    }

    public function removeCheckpoint(CheckpointContract $checkpoint)
    {
        foreach ($this->checkpoints as $key => $check) {
            if (get_class($check) === get_class($checkpoint)) {
                unset($checkpoints[$key]);
                break;
            }
        }

        return $this;
    }

    public function setCheckpointStatus($checkpoint_status)
    {
        $this->checkpoint_status = $checkpoint_status;

        return $this;
    }

    public function getCheckpointStatus()
    {
        return $this->checkpoint_status;
    }

    public function cycleCheckpoint(UserableContract $user)
    {
        if ($this->getCheckpointStatus() === false) {
            return true;
        } else {
            foreach ($this->checkpoints as $checkpoint) {
                $response = $checkpoint->login($user);

                if ($response === false) {
                    $fail = $checkpoint->messageFail($user);

                    throw new Exceptions\CheckpointException($fail);

                    return $response;
                }
            }

            return true;
        }
    }

    public function failedResponse($transalated)
    {
        throw new Exceptions\CredentialException(trans($transalated));
    }

    public function register(ProfileableContract $profile, array $credentials, $type = null)
    {
        $response = $this->fireEvent('registering', compact('profile', 'credentials', 'type'), true);

        if ($response === false) {
            return false;
        }

        $credentials    = new Credentials($credentials, $type);
        $identity       = $credentials->getIdentity();
        $identity_type  = $credentials->getIdentityType();
        $password       = $credentials->getPasswordHashed();

        if ($this->isExists($credentials)) {
            throw new Exceptions\AlreadyExistsException('Identitas ['.$identity.'] sudah terdaftar.');
        } else {
            $userIdentity           = $this->makeModel('user_identity');
            $userIdentity->identity = $identity;
            $userIdentity->type     = $identity_type;

            $user = $this->makeModel('user')->create(compact('password'));
            $user->identities()->save($userIdentity);

            $profile->users()->save($user);

            $this->fireEvent('registered', compact('user'));

            return $user;
        }
    }

    public function isExists(Credentials $credentials)
    {
        $userIdentity = $this->makeModel('user_identity');

        $has = $userIdentity->where('identity', $credentials->getIdentity())->count();

        return $has > 0 ? true : false;
    }

    protected function fireEvent($event, $payload = [], $halt = false)
    {
        $dispatcher = $this->dispatcher;
        $method     = $halt ? 'until' : 'fire';
        $event      = 'multiauthable.' . $event;
        
        return call_user_func_array([$dispatcher, $method], [$event, $payload]);
    }

    protected function makeModel($name)
    {
        $model_class = array_get($this->models, $name);

        return new $model_class;
    }
}