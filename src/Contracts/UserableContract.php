<?php

namespace Nurmanhabib\MultiAuthable\Contracts;

interface UserableContract
{
    public function getFullName();
    public function getFirstName();
    public function getLastName();
    public function getIdentities();
    public function getProfile();
}