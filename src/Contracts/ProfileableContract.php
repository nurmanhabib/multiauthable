<?php

namespace Nurmanhabib\MultiAuthable\Contracts;

interface ProfileableContract
{
    public function getIdentities();
    public function hasIdentity($type);
    public function hasUser();
    public function getUser();
    public function getFullName();
    public function getFirstName();
    public function getLastName();
}
