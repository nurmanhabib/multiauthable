<?php

namespace Nurmanhabib\MultiAuthable\Contracts;

interface IdentityContract
{
    public function match($identity);
    public function messageIfNotMatch($identity);
}