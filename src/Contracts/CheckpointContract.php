<?php

namespace Nurmanhabib\MultiAuthable\Contracts;

use Illuminate\Contracts\Auth\Authenticatable;

interface CheckpointContract
{
    public function login(Authenticatable $user);

    public function check(Authenticatable $user);

    public function messageFail(Authenticatable $user = null);
}