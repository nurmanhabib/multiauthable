<?php

namespace Nurmanhabib\MultiAuthable;

use Illuminate\Support\ServiceProvider;

use Nurmanhabib\MultiAuthable\MultiAuthable;
use Nurmanhabib\MultiAuthable\MultiAuthableFacade;

class MultiAuthableServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->registerFlash();
    }

    public function boot()
    {
        $this->registerConfig();
        $this->registerApp();
        $this->registerFacade();
        $this->registerMigration();
        // $this->registerRoute();
        $this->registerViews();
    }

    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../config/multiauthable.php' => config_path('multiauthable.php'),
        ], 'config');

        $this->mergeConfigFrom(
            __DIR__.'/../config/multiauthable.php', 'multiauthable'
        );

    }

    protected function registerApp()
    {
        $this->app->bind(MultiAuthable::class, function ($app) {
            $checkpoint_status  = $app['config']->get('multiauthable.checkpoint.enable', true);
            $checkpoints        = $app['config']->get('multiauthable.checkpoint.checkpoints', []);

            $auth = new MultiAuthable($app);
            $auth->setCheckpointStatus((bool) $checkpoint_status);

            foreach ($checkpoints as $checkpoint) {
                $instance = $app->make($checkpoint);

                $auth->addCheckpoint($instance);
            }

            return $auth;
        });

        $this->app->bind('multiauthable', MultiAuthable::class);
    }

    protected function registerFacade()
    {
        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
        $loader->alias('MultiAuthable', MultiAuthableFacade::class);
    }

    protected function registerMigration()
    {
        $this->publishes([
            __DIR__.'/../database/migrations' => database_path('migrations'),
        ], 'migrations');
    }

    protected function registerRoute()
    {
        if (! $this->app->routesAreCached()) {
            require __DIR__.'/Http/routes.php';
        }
    }

    protected function registerViews()
    {
        $this->loadViewsFrom(resource_path('views/auth'), 'auth');
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'auth');
    }

    protected function registerFlash()
    {
        $this->app->register(\Laracasts\Flash\FlashServiceProvider::class);
    }
}
