<?php

namespace Nurmanhabib\MultiAuthable\Identities;

use Nurmanhabib\MultiAuthable\Contracts\IdentityContract;

class Username implements IdentityContract
{
    public function match($identity)
    {
    	$match = preg_match("/^[a-z][a-z0-9]*(?:_[a-z0-9]+)*$/", $identity);

    	return $match ? true : false;
    }

    public function messageIfNotMatch($identity)
    {
        return 'Identitas ['.$identity.'] harus berupa huruf tanpa spasi.';
    }
}