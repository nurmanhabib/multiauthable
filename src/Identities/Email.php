<?php

namespace Nurmanhabib\MultiAuthable\Identities;

use Nurmanhabib\MultiAuthable\Contracts\IdentityContract;

class Email implements IdentityContract
{
    public function match($identity)
    {
    	return filter_var($identity, FILTER_VALIDATE_EMAIL);
    }

    public function messageIfNotMatch($identity)
    {
        return 'Identitas ['.$identity.'] harus berupa alamat email yang valid.';
    }
}