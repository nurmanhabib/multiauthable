<?php

namespace Nurmanhabib\MultiAuthable\Identities;

use Nurmanhabib\MultiAuthable\Contracts\IdentityContract;

class PhoneNumber implements IdentityContract
{
    public function match($identity)
    {
        try {
            $phone_util     = \libphonenumber\PhoneNumberUtil::getInstance();
            $phone_number   = $phone_util->parse($identity, 'ID');

            return $phone_util->isValidNumber($phone_number);
        } catch (\libphonenumber\NumberParseException $e) {
            return false;
        }
    }

    public function messageIfNotMatch($identity)
    {
        return 'Identitas ['.$identity.'] harus berupa nomer telepon.';
    }
}