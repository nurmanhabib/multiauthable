<?php

namespace Nurmanhabib\MultiAuthable;

class Credentials
{
    protected $identity;
    protected $identity_type;
    protected $password;

    public function __construct(array $credentials, $type = null)
    {
        $this->newInstance($credentials, $type);
    }

    public function newInstance(array $credentials, $type = null)
    {
        $default    = config('multiauthable.identity.default');
        $adapters   = config('multiauthable.identity.adapters');

        if (array_key_exists('type', $credentials)) {
            $type   = $credentials['type'];

            unset($credentials['type']);
        } else {
            $type   = $type ?: $default;
        }

        if ($type === 'auto') {
            if (array_key_exists('identity', $credentials)) {
                $identity = array_get($credentials, 'identity');

                foreach ($adapters as $type => $adapter) {
                    if (IdentityFactory::match($identity, $type)) {
                        return $this->newInstance($credentials, $type);
                    }
                }

                throw new Exceptions\CredentialException('Validasi identitas ['.$identity.'] tidak ada yang sesuai.'); 
            } else {
                throw new Exceptions\CredentialException('Tidak menemukan indeks [identity] pada credentials.');  
            }          
        } else {
            if (array_key_exists($type, $credentials) || array_key_exists('identity', $credentials)) {
                $identity   = array_get($credentials, $type, array_get($credentials, 'identity'));
                $factory    = IdentityFactory::make($type);
                
                if ($factory->match($identity, $type)) {
                    $this->identity         = $identity;
                    $this->identity_type    = $type;
                    $this->password         = array_get($credentials, 'password');

                    return $this;
                } else {
                    throw new Exceptions\CredentialException($factory->messageIfNotMatch($identity)); 
                }
            } else {
                throw new Exceptions\CredentialException('Tidak menemukan indeks ['.$type.'] pada credentials.');  
            }
        }
    }

    public function getIdentity()
    {
        return $this->identity;
    }

    public function getIdentityType()
    {
        return $this->identity_type;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getPasswordHashed()
    {
        return bcrypt($this->getPassword());
    }

    public function toArray()
    {
        $identity       = $this->getIdentity();
        $identity_type  = $this->getIdentityType();

        return compact('identity', 'identity_type');
    }

    public function toJson()
    {
        return json_encode($this->toArray());
    }

    public function __toString()
    {
        return $this->toJson();
    }
}
