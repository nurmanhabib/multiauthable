<?php

namespace Nurmanhabib\MultiAuthable\Checkpoints;

use Illuminate\Contracts\Auth\Authenticatable;
use Nurmanhabib\MultiAuthable\Contracts\CheckpointContract;
use Nurmanhabib\MultiAuthable\Exceptions\CheckpointException;

class BannedCheckpoint implements CheckpointContract
{
    public function login(Authenticatable $user)
    {
        return $this->check($user);
    }

    public function check(Authenticatable $user)
    {
        return $user->isBanned();
    }

    public function messageFail(Authenticatable $user = null)
    {
        $banned = $user->getBanned();

        return trans('auth.banned_' . $banned->type, ['seconds' => $banned->getDelay()]);
    }
}