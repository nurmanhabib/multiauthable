<?php

namespace Nurmanhabib\MultiAuthable\Checkpoints;

use Illuminate\Contracts\Auth\Authenticatable;
use Nurmanhabib\MultiAuthable\Contracts\CheckpointContract;
use Nurmanhabib\MultiAuthable\Exceptions\CheckpointException;

class ActivationCheckpoint implements CheckpointContract
{
    public function login(Authenticatable $user)
    {
        return $this->check($user);
    }

    public function check(Authenticatable $user)
    {
        $activation = $user->activation;

        if ($activation && $activation->completed) {
            return true;
        } else {
            return false;
        }
    }

    public function messageFail(Authenticatable $user = null)
    {
        return trans('auth.activation');
    }
}