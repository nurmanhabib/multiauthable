<?php

namespace Nurmanhabib\MultiAuthable;

class IdentityFactory
{
    public static function make($type = null)
    {
        $default    = config('multiauthable.identity.default');
        $adapters   = config('multiauthable.identity.adapters');
        $type       = $type ?: $default;
        $adapter    = array_get($adapters, $type);

        if ($adapter) {
            return new $adapter;
        } else {
            return null;
        }
    }

    public static function match($identity, $type = null)
    {
        $factory = self::make($type);

        return $factory->match($identity);
    }
}
