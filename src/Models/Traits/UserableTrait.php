<?php

namespace Nurmanhabib\MultiAuthable\Models\Traits;

trait UserableTrait
{
    public function getFullNameAttribute()
    {
        return $this->getFullName();
    }

    public function getFirstNameAttribute()
    {
        return $this->getFirstName();
    }

    public function getLastNameAttribute()
    {
        return $this->getLastName();
    }

    public function getFullName()
    {
        if ($this->hasProfile()) {
            return $this->getProfile()->getFullName();
        } else {
            return '';
        }
    }

    public function getFirstName()
    {
        if ($this->hasProfile()) {
            return $this->getProfile()->getFirstName();
        } else {
            return '';
        }
    }

    public function getLastName()
    {
        if ($this->hasProfile()) {
            return $this->getProfile()->getLastName();
        } else {
            return '';
        }
    }

    public function getIdentities()
    {
        return $this->identities;
    }

    public function identities()
    {
        $userIdentityRelated = config('multiauthable.models.user_identity');

        return $this->hasMany($userIdentityRelated);
    }

    public function hasProfile()
    {
        return $this->getProfile() ? true : false;
    }

    public function getProfile()
    {
        return $this->profileable;
    }

    public function profileable()
    {
        return $this->morphTo();
    }
}
