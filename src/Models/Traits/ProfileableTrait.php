<?php

namespace Nurmanhabib\MultiAuthable\Models\Traits;

trait ProfileableTrait
{
    public function users()
    {
        $userRelated = config('multiauthable.models.user');
        
        return $this->morphMany($userRelated, 'profileable');
    }

    public function getIdentities()
    {

    }

    public function hasIdentity($type)
    {
        $has = $this->users()->whereHas('identities', function ($query) use ($type) {
            $query->where('type', $type);
        })->count();

        return $has > 0 ? true : false;
    }

    public function hasUser()
    {
        return $this->users()->count() > 0;
    }

    public function getUser()
    {
        return $this->users()->first();
    }

    public function getFullName()
    {
        return trim($this->getFirstName().' '.$this->getLastName());
    }

    public function getFirstName()
    {
        return $this->first_name;
    }

    public function getLastName()
    {
        return $this->last_name;
    }
}