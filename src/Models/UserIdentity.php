<?php

namespace Nurmanhabib\MultiAuthable\Models;

use Illuminate\Database\Eloquent\Model;
use Nurmanhabib\MultiAuthable\Contracts\UserContract;

class UserIdentity extends Model
{
    public function user()
    {
        $userRelated = config('multiauthable.models.user');

        return $this->belongsTo($userRelated);
    }
}
