<?php

Route::group(['middleware' => 'web', 'namespace' => 'Nurmanhabib\MultiAuthable\Http\Controllers'], function () {
    Route::get('login', [
        'uses'  => 'LoginController@getLogin',
        'as'    => 'multiauthable.login'
    ]);

    Route::post('login', [
        'uses'  => 'LoginController@postLogin',
        'as'    => 'multiauthable.login.post'
    ]);

    Route::get('profile', [
        'uses'  => 'ProfileController@index',
        'as'    => 'multiauthable.profile'
    ]);

    Route::put('profile', [
        'uses'  => 'ProfileController@update',
        'as'    => 'multiauthable.profile.update'
    ]);
});
