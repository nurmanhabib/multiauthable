<?php

namespace Nurmanhabib\MultiAuthable\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers as BaseAuthenticatesUsers;        
use Illuminate\Support\Facades\Auth;

use Nurmanhabib\MultiAuthable\Exceptions\CredentialException;
use Nurmanhabib\MultiAuthable\MultiAuthableFacade as MultiAuthable;

trait AuthenticatesUsers
{
    use BaseAuthenticatesUsers;
    
    public function showLoginForm()
    {
        return themes('admin')->view('auth.login');
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($lockedOut = $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        try {
            $credentials    = $this->credentials($request);
            $remember       = $request->has('remember');
            $authenticate   = MultiAuthable::authenticate($credentials, null, $remember);

            return $this->sendLoginResponse($request);
        } catch (CredentialException $e) {
            // If the login attempt was unsuccessful we will increment the number of attempts
            // to login and redirect the user back to the login form. Of course, when this
            // user surpasses their maximum number of attempts they will get locked out.
            if (! $lockedOut) {
                $this->incrementLoginAttempts($request);
            }

            return $this->sendFailedLoginResponse($request, $e->getMessage());
        }
    }

    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            $this->identity() => 'required',
            'password' => 'required',
        ]);
    }

    protected function credentials(Request $request)
    {
        return $request->only($this->identity(), 'password');
    }

    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        return $this->authenticated($request, $this->guard()->user())
                ?: redirect()->intended($this->redirectPath());
    }

    protected function authenticated(Request $request, $user)
    {
        // 
    }

    protected function sendFailedLoginResponse(Request $request, $message = '')
    {
        flash($message, 'error')->important();

        return redirect()->back()
            ->withInput($request->only($this->identity(), 'remember'));
    }

    protected function username()
    {
        return $this->identity();
    }

    protected function identity()
    {
        return 'identity';
    }

    protected function guard()
    {
        return Auth::guard();
    }
}