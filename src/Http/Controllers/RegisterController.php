<?php

namespace Nurmanhabib\MultiAuthable\Http\Controllers;

use Illuminate\Http\Request;
use Nurmanhabib\MultiAuthable\MultiAuthableFacade as MultiAuthable;

class LoginController extends Controller
{
    use RedirectsUsers;

    public function getRegister()
    {
        return view('auth::register');
    }

    public function postRegister(Request $request)
    {
        $this->validate($request, [
            'identity' => 'required',
            'password' => 'required',
        ]);

        // Registering...
    }
}