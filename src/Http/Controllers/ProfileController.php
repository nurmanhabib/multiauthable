<?php

namespace Nurmanhabib\MultiAuthable\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Authenticatable;
use Nurmanhabib\MultiAuthable\Exceptions\CredentialException;
use Nurmanhabib\MultiAuthable\MultiAuthableFacade as MultiAuthable;

class ProfileController extends Controller
{
	public function index(Authenticatable $user)
	{
		$user->load('profileable');

		return view('auth::profile.edit', compact('user'));
	}

	public function update(Request $request, Authenticatable $user)
	{
		$user->updateProfile($request);

		flash('Profile berhasil diperbarui.', 'success')->important();

		return redirect()->back();
	}
}
